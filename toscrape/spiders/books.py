# -*- coding: utf-8 -*-

import scrapy

import toscrape.items


class BooksSpider(scrapy.Spider):
    name = "books"
    allowed_domains = ["books.toscrape.com"]
    start_urls = ["http://books.toscrape.com/"]

    def parse(self, response):
        items = response.css(".nav > li:nth-child(1) > ul:nth-child(2) li a")
        for link in items:
            next_url = link.css("a::attr(href)").extract_first()
            if next_url:
                yield response.follow(next_url, callback=self.parse_category)

    def parse_category(self, response):
        category = response.css(".page-header > h1:nth-child(1)::text")
        category = category.extract_first()

        items = response.css(
            "section li.col-xs-6 > article > h3 > a::attr(href)"
        )
        for item in items:
            request = scrapy.Request(
                response.urljoin(item.extract()), callback=self.parse_book
            )
            request.meta["book_category"] = category
            yield request

        for page in response.css(".next > a::attr(href)").extract():
            yield response.follow(page, callback=self.parse_category)

    def parse_book(self, response):
        loader = toscrape.items.BookLoader(
            item=toscrape.items.BookItem(), response=response
        )

        loader.add_css("title", "div.product_main > h1")
        loader.add_css(
            "rating", "div.product_main > p.star-rating::attr(class)"
        )
        loader.add_xpath(
            "description", "/html/body/div/div/div[2]/div[2]/article/p"
        )
        loader.add_css("upc", "article.product_page tr:nth-child(1) td")
        loader.add_css("price", "article.product_page tr:nth-child(3) td")
        loader.add_css("tax", "article.product_page tr:nth-child(5) td")
        loader.add_css("available", "article.product_page tr:nth-child(6) td")

        yield loader.load_item()
