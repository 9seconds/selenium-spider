# -*- coding: utf-8 -*-

import decimal
import re

import scrapy
import scrapy.loader
import scrapy.loader.processors
import w3lib.html


BOOK_RATINGS = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5}


def extract_rating(value):
    value = value.replace("star-rating", "")
    value = value.strip()
    value = value.lower()

    return BOOK_RATINGS.get(value)


def extract_price(value):
    price = re.findall(r"\d+(?:\.\d+)?", value)
    if len(price) == 1:
        return decimal.Decimal(price[0])


def extract_available(value):
    matcher = re.match(r"In stock \((\d+) available\)", value)
    if matcher:
        return int(matcher.group(1))

    return 0


class ToscrapeItem(scrapy.Item):
    pass


class BookItem(ToscrapeItem):
    title = scrapy.Field()
    rating = scrapy.Field(
        input_processor=scrapy.loader.processors.
        MapCompose(w3lib.html.remove_tags, extract_rating)
    )
    description = scrapy.Field()
    upc = scrapy.Field()
    price = scrapy.Field(
        input_processor=scrapy.loader.processors.
        MapCompose(w3lib.html.remove_tags, extract_price)
    )
    tax = scrapy.Field(
        input_processor=scrapy.loader.processors.
        MapCompose(w3lib.html.remove_tags, extract_price)
    )
    available = scrapy.Field(
        input_processor=scrapy.loader.processors.
        MapCompose(w3lib.html.remove_tags, extract_available)
    )


class BookLoader(scrapy.loader.ItemLoader):

    default_output_processor = scrapy.loader.processors.TakeFirst()
    default_input_processor = scrapy.loader.processors.MapCompose(
        w3lib.html.remove_tags
    )
