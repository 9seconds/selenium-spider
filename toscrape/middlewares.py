# -*- coding: utf-8 -*-

import scrapy.exceptions
import scrapy.http
import scrapy.signals
import scrapy.utils.python
import selenium.webdriver
import selenium.webdriver.common.proxy


class SeleniumMiddleware:

    @classmethod
    def from_crawler(cls, crawler):
        grid_url = crawler.settings.get("SELENIUM_GRID_URL")
        headless_proxy_addr = crawler.settings.get(
            "SELENIUM_HEADLESS_PROXY_ADDRESS"
        )
        if "SELENIUM_GRID_URL" not in crawler.settings:
            raise scrapy.exceptions.NotConfigured(
                "SELENIUM_GRID_URL has to be set"
            )
        if "SELENIUM_HEADLESS_PROXY_ADDRESS" not in crawler.settings:
            raise scrapy.exceptions.NotConfigured(
                "SELENIUM_HEADLESS_PROXY_ADDRESS has to be set"
            )
        if "SELENIUM_DESIRED_CAPABILITIES" not in crawler.settings:
            raise scrapy.exceptions.NotConfigured(
                "SELENIUM_DESIRED_CAPABILITIES has to be set"
            )

        ware = cls(
            crawler.settings["SELENIUM_GRID_URL"],
            crawler.settings["SELENIUM_HEADLESS_PROXY_ADDRESS"],
            crawler.settings["SELENIUM_DESIRED_CAPABILITIES"]
        )
        crawler.signals.connect(
            ware.spider_opened, scrapy.signals.spider_opened)
        crawler.signals.connect(
            ware.spider_closed, scrapy.signals.spider_closed)

        return ware

    def __init__(self, grid_url, proxy_addr, capabilities):
        proxy = selenium.webdriver.common.proxy.Proxy()
        proxy.http_proxy = proxy_addr
        proxy.ftp_proxy = proxy_addr
        proxy.sslProxy = proxy_addr
        proxy.no_proxy = None
        proxy.proxy_type = selenium.webdriver.common.proxy.ProxyType.MANUAL
        proxy.add_to_capabilities(capabilities)
        capabilities["acceptSslCerts"] = True

        self.grid_url = grid_url
        self.capabilities = capabilities
        self.driver = None

    def spider_opened(self):
        self.driver = selenium.webdriver.Remote(
            command_executor=self.grid_url,
            desired_capabilities=self.capabilities,
            keep_alive=True
        )

    def spider_closed(self):
        if self.driver:
            self.driver.quit()
            self.driver = None

    def process_request(self, request, spider):
        request.meta["driver"] = self.driver

        self.driver.get(request.url)
        body = scrapy.utils.python.to_bytes(self.driver.page_source)

        return scrapy.http.HtmlResponse(
            self.driver.current_url,
            body=body,
            encoding="utf-8",
            request=request
        )
